def area_circle(radius):
    if type(radius) not in [int, float]:
        raise TypeError("радиус должен быть числом больше нуля")
    if radius < 0:
        raise ValueError("радиус должен быть больше нуля")
    area = radius * radius * 3.14
    return area
def area_triangle(side1, side2, side3):
    if type(side1) not in [int, float] or type(side2) not in [int, float] or type(side3) not in [int, float]:
        raise TypeError("значение длины стороны треугольника должно быть числовым")
    if side1 <= 0 or side2 <= 0 or side3 <= 0:
        raise ValueError("стороны треугольника должны быть больше нуля")
    semi_perimeter = (side1 + side2 + side3) / 2
    area = pow(semi_perimeter*(semi_perimeter - side1) * (semi_perimeter - side2) * (semi_perimeter - side3), 0.5)
    return area