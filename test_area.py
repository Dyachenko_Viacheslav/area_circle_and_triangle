import unittest
from area import area_circle, area_triangle
class TestArea(unittest.TestCase):
    def test_area(self):
        self.assertEqual(area_circle(5), 5*5*3.14)
        self.assertEqual(area_circle(1), 3.14)
        self.assertEqual(area_circle(0), 0)
        self.assertEqual(area_circle(5.1), 5.1*5.1*3.14)
    def test_values(self):
        self.assertRaises(ValueError, area_circle, -2)
    def test_types(self):
        self.assertRaises(TypeError, area_circle, "шесть")
        self.assertRaises(TypeError, area_circle, [2, 6])
    def test_value_triangle(self):
        self.assertRaises(ValueError, area_triangle, -2, -2, -2)
        self.assertRaises(ValueError, area_triangle, -2, 2, -2)
    def test_type_triangle(self):
        self.assertRaises(TypeError, area_triangle, "evj", [3, 5], 7)